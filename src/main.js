var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.1
var dimension = 9
var boardArray = create2DArray(dimension, dimension, 0, false)
var direction = 0

var player = {
  x: 0,
  y: 0
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < boardArray.length; i++) {
    for (var j = 0; j < boardArray[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(boardArray.length * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(boardArray[i].length * 0.5)) * boardSize * initSize)
      if (boardArray[i][j] === 1) {
        fill(0)
        noStroke()
        rect(0, 0, boardSize * initSize * 0.95, boardSize * initSize * 0.95)
      }
      fill(128 + 128 * sin(frameCount * 0.005 * (i - Math.floor(dimension * 0.5)) * (j - Math.floor(dimension * 0.5))))
      if (boardArray[i][j] === 0) {
        noStroke()
        rect(0, 0, boardSize * initSize * 0.95, boardSize * initSize * 0.95)
      }
      if (boardArray[i][j] === 2) {
        noStroke()
        beginShape()
        vertex(-boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        vertex(boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        vertex(-boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        endShape(CLOSE)
      }
      if (boardArray[i][j] === 3) {
        noStroke()
        beginShape()
        vertex(boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        vertex(-boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        vertex(boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        endShape(CLOSE)
      }
      if (boardArray[i][j] === 4) {
        noStroke()
        beginShape()
        vertex(-boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        vertex(boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        vertex(boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        endShape(CLOSE)
      }
      if (boardArray[i][j] === 5) {
        noStroke()
        beginShape()
        vertex(-boardSize * initSize * 0.475, -boardSize * initSize * 0.475)
        vertex(boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        vertex(-boardSize * initSize * 0.475, boardSize * initSize * 0.475)
        endShape(CLOSE)
      }
      if (boardArray[i][j] === 6) {
        noStroke()
        ellipse(0, 0, boardSize * initSize * 0.95)
      }
      pop()
    }
  }

  if (Math.floor(7 * abs(tan(frameCount * Math.random()))) % 7 === 0) {
    direction = Math.floor(Math.random() * 4)
  }
  if (direction === 0) {
    if (player.y < 1) {
      player.y = dimension - 1
    } else {
      player.y--
    }
  }
  if (direction === 1) {
    if (player.y > dimension - 2) {
      player.y = 0
    } else {
      player.y++
    }
  }
  if (direction === 2) {
    if (player.x < 1) {
      player.x = dimension - 1
    } else {
      player.x--
    }
  }
  if (direction === 3) {
    if (player.x > dimension - 2) {
      player.x = 0
    } else {
      player.x++
    }
  }
  boardArray[player.x][player.y] = (boardArray[player.x][player.y] + 1) % 7
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 7)
      }
    }
    array[i] = columns
  }
  return array
}
